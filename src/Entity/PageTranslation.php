<?php

namespace Webcube\Page\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="page_translation")
 */
class PageTranslation
{
    use \Knp\DoctrineBehaviors\Model\Translatable\Translation;
    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    protected $name;
}