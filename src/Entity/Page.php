<?php

namespace Webcube\Page\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="page")
 *
 * @method PageTranslation translate($lang='')
 */
class Page
{
    use \Knp\DoctrineBehaviors\Model\Translatable\Translatable,
        \Zenify\DoctrineBehaviors\Entities\Attributes\Translatable;

    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="guid", unique=true, options={"fixed" = true})
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    public function __construct(string $uuid = null)
    {
        $this->id = $uuid ? $uuid : (\Ramsey\Uuid\Uuid::uuid4())->getHex();
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
    }
}